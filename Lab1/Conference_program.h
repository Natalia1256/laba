#ifndef Conference_program_H
#define Conference_program_H
#include "constants.h"

struct date
{
    int hours;
    int minutes;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct conference_program
{
    date start;
    date finish;
    person author;
    char text[MAX_STRING_SIZE];
};
#endif