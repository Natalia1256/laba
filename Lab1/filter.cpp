#include "filter.h"
#include <cstring>
#include <iostream>

conference_program** filter(conference_program* array[], int size, bool (*check)(conference_program* element), int& result_size)
{
	conference_program** result = new conference_program * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_conference_program_by_author(conference_program* element)
{
	return strcmp(element->author.first_name, "Иван") == 0 &&
		strcmp(element->author.middle_name, "Иванович") == 0 &&
		strcmp(element->author.last_name, "Иванов") == 0;
}

bool check_conference_program_by_minute(conference_program* element)
{
	return element->start.hours == element->finish.hours && element->finish.minutes > element->start.minutes + 16 ||
		element->start.hours < element->finish.hours&& element->finish.minutes + 60 - element->start.minutes > 15;
}

