#include <iostream>
#include <iomanip>

using namespace std;

#include "Conference_program.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(conference_program* program)
{
	/********** ����� ����� **********/
	cout << "�����...........: ";
	// ����� ������� ������
	cout << program->author.last_name << " ";
	// ����� ������ ����� ����� ������
	cout << program->author.first_name[0] << ". ";
	// ����� ������ ����� �������� ������
	cout << program->author.middle_name[0] << ".";
	cout << ", ";
	// ����� ��������
	cout << '"' << program->text << '"';
	cout << '\n';
	/********** ����� ������  **********/
	// ����� �����
	cout << "������.....: ";
	cout << setw(2) << setfill('0') << program->start.hours << ':';
	// ����� ������
	cout << setw(2) << setfill('0') << program->start.minutes;
	cout << '\n';
	/********** ����� ��������� **********/
	// ����� �����
	cout << "���������...: ";
	cout << setw(2) << setfill('0') << program->finish.hours << ':';
	// ����� �����
	cout << setw(2) << setfill('0') << program->finish.minutes;
	cout << '\n';
	cout << '\n';
}

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �1. GIT\n";
	cout << "������� �2. ��������� �����������\n";
	cout << "�����: ������� �������\n";
	cout << "������: 14\n\n";
	conference_program* program[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", program, size);
		cout << "***** ��������� ����������� *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(program[i]);
		}
		bool (*check_function)(conference_program*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
														   // � ����������� � �������� ��������� �������� ���� book_subscription*
		cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
		cout << "1) ������� ��� ������� ������� ����� ���������\n";
		cout << "2) ������� ��� ������� ������������� ������ 15 �����\n";
		cout << "\n������� ����� ���������� ������: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_conference_program_by_author; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** �������� ��� ������� ������� ����� ��������� *****\n\n";
			break;
		case 2:
			check_function = check_conference_program_by_minute; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ������� ��� ������� ������������� ������ 15 ����� *****\n\n";
			break;
		default:
			throw "������������ ����� ������";
		}
		if (check_function)
		{
			int new_size;
			conference_program** filtered = filter(program, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete program[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}